package com.example.acer_pc.mobilemenu;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URI;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

public class ViewMenu extends ListActivity {

    static final ArrayList<HashMap<String, String>> list = new ArrayList<HashMap<String, String>>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.viewmenu_layout);

        SimpleAdapter adapter = new SimpleAdapter(this, list, R.layout.row_layout,
                new String[]{"pen", "price", "color"}, new int[]{R.id.food, R.id.desc, R.id.price, R.id.qty});

        populateList();

        setListAdapter(adapter);

    }


    private void populateList() {
        HashMap<String, String> temp = new HashMap<String, String>();
        temp.put("pen", "MONT Blanc");
        temp.put("price", "200.00$");
        temp.put("color", "Silver, Grey, Black");
        list.add(temp);
        HashMap<String, String> temp1 = new HashMap<String, String>();
        temp1.put("pen", "Gucci");
        temp1.put("price", "300.00$");
        temp1.put("color", "Gold, Red");
        list.add(temp1);
        HashMap<String, String> temp2 = new HashMap<String, String>();
        temp2.put("pen", "Parker");
        temp2.put("price", "400.00$");
        temp2.put("color", "Gold, Blue");
        list.add(temp2);
        HashMap<String, String> temp3 = new HashMap<String, String>();
        temp3.put("pen", "Sailor");
        temp3.put("price", "500.00$");
        temp3.put("color", "Silver");
        list.add(temp3);
        HashMap<String, String> temp4 = new HashMap<String, String>();
        temp4.put("pen", "Porsche Design");
        temp4.put("price", "600.00$");
        temp4.put("color", "Silver, Grey, Red");
        list.add(temp4);
    }

}