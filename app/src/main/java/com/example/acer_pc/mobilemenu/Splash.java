package com.example.acer_pc.mobilemenu;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

public class Splash extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_main);


        Thread logoTimer = new Thread() {
            @Override
            public void run() {
                try {
                    sleep(4000);
                    Intent i = new Intent("android.intent.action.ENTRY");
                    startActivity(i);
                }
                catch (InterruptedException e) {
                    e.printStackTrace();
                }
                finally {
                    finish();

                }
            }
        };

        logoTimer.start();

    }

}
